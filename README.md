# Fibonacci Sequence Generators

### Python
```python
def get_fib(n):
    return n if n == 0 or n == 1 else get_fib(n-1) + get_fib(n-2)
```

### Javascript

```javascript
function get_fib(n){
    return (n == 0 || n == 1) ? n : get_fib(n-1) + get_fib(n-2);
}
```

### PHP
```php
function get_fib($n){
    return ($n == 0 || $n == 1) ? $n : get_fib($n-1) + get_fib($n-2);
}
```
